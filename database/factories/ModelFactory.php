<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    $age = $faker->randomDigit;

    if ($age < 10)
        $age+= 10;

    return [
        'name' => $faker->name,
        'age' => $age,
        'email' => $faker->safeEmail,
        'password' => bcrypt('member'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Book::class, function (Faker\Generator $faker) {
    $quantity = $faker->randomDigit;

    if ($quantity < 10)
        $quantity+= 10;

    return [
        'title' => $faker->sentence(),
        'author' => $faker->name,
        'isbn' => $faker->isbn10,
        'quantity' => $quantity,
        'location' => 'Shelf - '. $faker->randomDigit,
    ];
});
