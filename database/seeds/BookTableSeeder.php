<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('books')->truncate();
        \DB::table('borrowers')->truncate();
        
        $books = factory(App\Models\Book::class, 20)->create();
    }
}
