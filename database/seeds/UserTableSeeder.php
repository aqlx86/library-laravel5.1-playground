<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'name' => 'Admin',
                'age' => 99,
                'email' => 'admin@library.dev',
                'password' => bcrypt('admin'),
                'is_admin' => true
            ],
        ];

        \DB::table('users')->truncate();
        \DB::table('users')->insert($users);

        $users = factory(App\Models\User::class, 5)->create();
    }
}
