#!/bin/sh

composer install
php artisan key:generate
php artisan clear-compiled
php artisan cache:clear
php artisan view:clear
php artisan config:clear
php artisan config:cache