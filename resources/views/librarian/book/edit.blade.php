@extends('layouts.master')

@section('title', 'Update Book')

@section('content')
    <h2>Update Book</h2>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($book, [
        'method' => 'PUT',
        'route' => ['librarian book update', $book['id']]
    ]) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('author', 'Author:', ['class' => 'control-label']) !!}
        {!! Form::text('author', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('isbn', 'ISBN:', ['class' => 'control-label']) !!}
        {!! Form::text('isbn', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('quantity', 'Quantity:', ['class' => 'control-label']) !!}
        {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('location', 'Location:', ['class' => 'control-label']) !!}
        {!! Form::text('location', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Update Book', ['class' => 'btn btn-primary']) !!} <a href="{{ route('librarian book listing') }}">cancel</a>

    {!! Form::close() !!}

@endsection