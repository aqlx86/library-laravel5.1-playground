@extends('layouts.master')

@section('title', 'Books')

@section('content')
    <div class="page-header">
        <h1>Books</h1>

        <a class="btn btn-primary" href="{{ route('librarian book create') }}">create</a>
    </div>

    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>ISBN</th>
            <th>Quantity</th>
            <th>Stock</th>
            <th>Borrowed</th>
            <th>Location</th>
            <th colspan="2">&nbsp;</th>
        </tr>

        @foreach($books as $book)
            <tr>
                <td>{{ $book->id }}</td>
                <td>
                    <a href='{{ route("librarian book borrower", ['id' => $book->id]) }}'>{{ $book->title }}</a>
                </td>
                <td>{{ $book->author }}</td>
                <td>{{ $book->isbn }}</td>
                <td>{{ ($book->quantity + $book->borrow_count) }}</td>
                <td>{{ $book->quantity }}</td>
                <td>
                    <a href='{{ route("librarian book borrower", ['id' => $book->id]) }}'>{{ $book->borrow_count }}</a>
                </td>
                <td>{{ $book->location }}</td>
                <td>
                    <a href="{{ route('librarian book edit', $book['id']) }}" class="btn btn-xs btn-primary">edit</a>
                </td>
                <td>
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['librarian book delete', $book['id']]
                    ]) !!}
                        {!! Form::submit('delete', ['class' => 'btn btn-xs btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

    {!! $books->render() !!}

@endsection
