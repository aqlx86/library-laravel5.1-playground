@extends('layouts.master')

@section('title', 'Borrowers - {{ $book->title }}')

@section('content')
    <div class="page-header">
        <h1>{{ $book->title }}</h1>
        <p>By: {{ $book->author }}, ISBN: {{ $book->isbn }}</p>
        <p>Stock:{{ $book->quantity }}  </p>
    </div>

    <h2>Borrowers</h2>

    <table class="table table-bordered">
        <tr>
            <th>Member</th>
            <th>Date Borrowed</th>
            <th>Expire At</th>
            <th>Penalty</th>
        </tr>

        @foreach($borrowers as $borrower)
            <tr>
                <td>{{ $borrower->member->name }}</td>
                <td>{{ $borrower->created_at }}</td>
                <td>{{ $borrower->expired_at }}</td>
                <td>{{ Library\Helper\LoanHelper::compute_penalty($borrower->expired_at) }} USD </td>
            </tr>
        @endforeach
    </table>

@endsection
