@extends('layouts.master')

@section('title', 'Book Loans')

@section('content')
    <div class="page-header">
        <h1>Book Loans</h1>
    </div>

    <table class="table table-bordered">
        <tr>
            <th>Title</th>
            <th>Member</th>
            <th>Date Borrowed</th>
            <th>Expire At</th>
            <th>Penalty</th>
        </tr>

        @foreach($loans as $loan)
            <tr>
                <td>
                    @if ($loan->book)
                        {{ $loan->book->title }}
                    @else
                        -- missing --
                    @endif
                </td>
                <td>{{ $loan->member->name }}</td>
                <td>{{ $loan->created_at }}</td>
                <td>{{ $loan->expired_at }}</td>
                <td>{{ Library\Helper\LoanHelper::compute_penalty($loan->expired_at) }} USD </td>
            </tr>
        @endforeach
    </table>

@endsection
