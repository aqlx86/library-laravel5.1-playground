@extends('layouts.master')

@section('title', 'Members')

@section('content')
    <div class="page-header">
        <h1>Members</h1>
        
        <a class="btn btn-primary" href="{{ route('librarian member create') }}">create</a>
    </div>

    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Age</th>
            <th>Email</th>
            <th colspan="2">&nbsp;</th>
        </tr>

        @foreach($members as $member)
            <tr>
                <td>{{ $member['id'] }}</td>
                <td>{{ $member['name'] }}</td>
                <td>{{ $member['age'] }}</td>
                <td>{{ $member['email'] }}</td>
                <td>
                    <a href="{{ route('librarian member edit', $member['id']) }}" class="btn btn-xs btn-primary">edit</a>
                </td>
                <td>
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['librarian member delete', $member['id']]
                    ]) !!}
                        {!! Form::submit('delete', ['class' => 'btn btn-xs btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

    {!! $members->render() !!}
@endsection