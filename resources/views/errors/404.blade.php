@extends('layouts.master')

@section('title', '404 - Page not found')

@section('content')
    <div class="page-header">
        <h1>404 - Page not found</h1>
    </div>
@endsection
