@extends('layouts.auth')

@section('title', 'Login')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="form-signin" role="form" method="POST" action="{{ route('login form') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <h2 class="form-signin-heading">Please sign in</h2>

        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name='email' placeholder="Email">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name='password' placeholder="Password">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>

        <div class="form-group">
            <p class="help-block">
                Admin Login: <mark>admin@library.dev</mark> <br />
                Admin Password: <mark>admin</mark>
            </p>

            <p class='help-block'>
                Pre-loaded members uses the password <mark>"member"</mark>
            </p>
        </div>

    </form>

@endsection
