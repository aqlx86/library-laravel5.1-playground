@extends('layouts.master')

@section('title', 'Available Books')

@section('content')
    <div class="page-header">
        <h1>Available Books</h1>
    </div>


    {!! Form::open([
        'method' => 'GET',
        'route' => ['book listing']
    ]) !!}

        <div class="form-group">
            {!! Form::text('query', $query, ['class' => 'form-control',
                'placeholder' => 'Enter book title, author or ISBN']) !!}


        </div>

        <div class="form-group form-inline">
            <button type="submit" class="btn btn-default">search</button>

            <div class="checkbox">
                <label>{!! Form::checkbox('borrowed', '1', $borrowed) !!} Show only borrowed</label>
              </div>
        </div>

    {!! Form::close() !!}

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>ISBN</th>
                    <th>Stock</th>
                    <th>Location</th>
                    <th>&nbsp;</th>
                </tr>

                @foreach($books as $book)
                    <tr>
                        <td>{{ $book->id }}</td>
                        <td>{{ $book->title }}</td>
                        <td>{{ $book->author }}</td>
                        <td>{{ $book->isbn }}</td>
                        <td>{{ $book->quantity }}</td>
                        <td>{{ $book->location }}</td>
                        <td>
                            @if ($book['is_borrowed'])
                                <a href="{{ route('book surrender', $book['id']) }}" class="btn btn-xs btn-warning">return</a>
                            @else
                                <a href="{{ route('book borrow', $book['id']) }}" class="btn btn-xs btn-primary">borrow</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>

            {!! $books->appends(Input::all())->render() !!}

        </div>
    </div>

@endsection
