@extends('layouts.master')

@section('title', 'Borrow Book - {{ $book->title }}')

@section('content')
    <div class="page-header">
        <h1>{{ $book->title }}</h1>
        <p>By: {{ $book->author }}, ISBN: {{ $book->isbn }}</p>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open([
        'method' => 'POST',
        'route' => ['book borrow', $book['id']]
    ]) !!}
        {!! Form::submit('Borrow', ['class' => 'btn btn-primary']) !!}  <a href="{{ route('book listing') }}">cancel</a>
    {!! Form::close() !!}

@endsection