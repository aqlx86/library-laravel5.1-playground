<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Library\Repository\Book as BookRepository;
use Carbon\Carbon;

class Borrower extends Model implements
    BookRepository\Borrow, BookRepository\Surrender
{
    protected $table = 'borrowers';

    protected $fillable = ['book_id', 'user_id', 'expired_at'];

    public function borrow_book($user_id, $book_id)
    {
        $expired_at = Carbon::now()
            ->addWeeks(2);

        //dd ($expired_at->format('Y-m-d h:i:s'));

        $this->create([
            'user_id' => $user_id,
            'book_id' => $book_id,
            'expired_at' => $expired_at->format('Y-m-d h:i:s')
        ]);
    }

    public function book()
    {
        return $this->belongsTo('App\Models\Book');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function surrender_book($user_id, $book_id)
    {
        $book = $this->where('user_id', '=', $user_id)
            ->where('book_id', '=', $book_id)
            ->delete();
    }

    public function count_book_in_hand($user_id)
    {
        return $this->where('user_id', '=', $user_id)->count();
    }
}
