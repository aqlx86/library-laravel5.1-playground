<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Library\Repository;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract,
                                    Repository\User
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';

    protected $fillable = ['name', 'age', 'email', 'password', 'is_admin'];

    protected $hidden = ['password', 'remember_token'];

    public function get_user($user_id)
    {
        return $this->find($user_id)->toArray();
    }

    public function create_user(array $user)
    {
        $this->create([
            'name' => $user['name'],
            'age' => $user['age'],
            'email' => $user['email'],
            'password' => bcrypt($user['password']),
        ]);
    }

    public function update_user($id, array $user)
    {
        $this->where('id', $id)->update($user);
    }
}
