<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Library\Repository;

class Book extends Model implements Repository\Book
{
    protected $table = 'books';

    protected $fillable = ['title', 'author', 'isbn', 'quantity', 'location'];

    public function create_book(array $book)
    {
        $this->create([
            'title' => $book['title'],
            'author' => $book['author'],
            'isbn' => $book['isbn'],
            'quantity' => $book['quantity'],
            'location' => $book['location']
        ]);
    }

    public function update_book($id, array $book)
    {
        $this->where('id', $id)
            ->update([
                'title' => $book['title'],
                'author' => $book['author'],
                'isbn' => $book['isbn'],
                'quantity' => $book['quantity'],
                'location' => $book['location']
            ]);
    }

    public function get_book($book_id)
    {
        return $this->find($book_id)->toArray();
    }

    public function increase_quantity($book_id, $count = 1)
    {
        $this->where('id', '=', $book_id)
            ->increment('quantity', $count);
    }

    public function decrease_quantity($book_id, $count = 1)
    {
        $this->where('id', '=', $book_id)
            ->decrement('quantity', $count);
    }
}
