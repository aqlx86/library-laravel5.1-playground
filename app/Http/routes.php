<?php

Route::get('/', [
    'uses' => 'DashboardController@index',
    'middleware' => ['auth']
]);

Route::get('auth/login', [
    'as' => 'login form',
    'uses' => 'Auth\AuthController@getLogin'
]);
Route::post('auth/login', 'Auth\AuthController@postLogin');

Route::get('auth/logout', [
    'as' => 'auth logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

require_once app_path() . '/Http/Routes/librarian.php';
require_once app_path() . '/Http/Routes/student.php';