<?php

namespace App\Http\Controllers\Librarian;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Book as BookModel;
use App\Models\Borrower as BorrowerModel;

use Library;
use Library\Usecase\Book\Create as BookCreateUsecase;
use Library\Usecase\Book\Update as BookUpdateUsecase;

class BookController extends Controller
{
    /**
     * @todo remove here
     */
    public function listing(BookModel $book)
    {
        $books = $book
            ->select(\DB::raw('books.*, (SELECT count(b.book_id) from borrowers as b where books.id = b.book_id) as borrow_count'))
            ->paginate(10);

        return view('librarian.book.listing', [
            'books' => $books
        ]);
    }

    /**
     * @todo remove here
     */
    public function loan_listing(Request $request, BorrowerModel $model)
    {
        $loans = $model
            ->with(['book', 'member'])
            ->get();

        return view('librarian.book.loan_listing', [
            'loans' => $loans
        ]);
    }

    public function index()
    {
        return view('librarian.book.index');
    }

    public function store(Request $request)
    {
        $usecase = new BookCreateUsecase(
            new BookModel
        );

        $validator = new Library\Validator\Book;
        $validator->setup($request->all());

        if (! $validator->create()->is_valid())
            $this->throwValidationException($request, $validator->get_validator());

        $usecase->handle($request->all());

        return redirect(route('librarian book listing'));
    }

    public function edit($id, BookModel $book)
    {
        return view('librarian.book.edit', [
            'book' => $book->findOrFail($id)
        ]);
    }

    public function update($id, Request $request)
    {
        $usecase = new BookUpdateUsecase(
            new BookModel
        );

        $validator = new Library\Validator\Book;
        $validator->setup($request->all());
        $validator->add_input('id', $id);

        if (! $validator->update()->is_valid())
            $this->throwValidationException($request, $validator->get_validator());

        $usecase->handle($id, $request->all());

        return redirect(route('librarian book listing'));
    }

    public function destroy($id, BookModel $book)
    {
        $book = $book->findOrFail($id);

        $book->delete();

        return redirect(route('librarian book listing'));
    }

    /**
     * @todo remove here
     */
    public function borrowers($id, Request $request)
    {
        $book = BookModel::findOrFail($id);
        $borrowers = BorrowerModel::where('book_id', '=', $id)
            ->with(['book', 'member'])
            ->get();

        return view('librarian.book.borrower_listing', [
            'book' => $book,
            'borrowers' => $borrowers
        ]);
    }
}
