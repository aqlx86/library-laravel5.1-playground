<?php

namespace App\Http\Controllers\Librarian;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User as MemberModel;

use Library;
use Library\Usecase;

class MemberController extends Controller
{
    use RegistersUsers;

    public function listing(MemberModel $member)
    {
        $members = $member
            ->where('id', '!=', 1)
            ->paginate(10);

        return view('librarian.member.listing', [
            'members' => $members
        ]);
    }

    public function index()
    {
        return view('librarian.member.index');
    }

    public function store(Request $request)
    {
        $validator = new Library\Validator\Member;
        $validator->setup($request->all());

        if (! $validator->create()->is_valid())
            $this->throwValidationException($request, $validator->get_validator());

        $usecase = new Usecase\Member\Create(
            new MemberModel
        );

        $usecase->handle($request->all());

        return redirect(route('librarian member listing'));
    }

    public function edit($id, MemberModel $member)
    {
        $member = $member->findOrFail($id);

        return view('librarian.member.edit', [
            'member' => $member
        ]);
    }

    public function update($id, Request $request)
    {
        $validator = new Library\Validator\Member;
        $validator->setup($request->all());
        $validator->add_input('id', $id);

        if (! $validator->update()->is_valid())
            $this->throwValidationException($request, $validator->get_validator());

        $usecase = new Usecase\Member\Update(
            new MemberModel
        );

        $usecase->handle($id, $request->all());

        return redirect(route('librarian member listing'));
    }

    public function destroy($id, MemberModel $member)
    {
        $member = $member->findOrFail($id);

        $member->delete();

        return redirect(route('librarian member listing'));
    }
}
