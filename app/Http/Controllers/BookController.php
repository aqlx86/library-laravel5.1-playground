<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Book as BookModel;
use App\Models\Borrower as BorrowerModel;
use App\Models\User as UserModel;

use Library\Usecase\Book\Borrow as BorrowUsecase;
use Library\Usecase\Book\Surrender as SurrenderUsecase;

class BookController extends Controller
{
    /**
     * @todo remove here
     */
    public function listing(BookModel $book, Request $request)
    {
        $member_id = \Auth::user()->id;

        $query = $request->get('query', null);
        $borrowed = $request->get('borrowed', null);

        $books = $book
            ->select(\DB::raw("books.*, (SELECT count(b.book_id) from borrowers as b where books.id = b.book_id and b.user_id = $member_id) as is_borrowed"))
            ->orWhere(function($q) use ($query) {
                $q->where('title', 'like', "%$query%", 'OR');
                $q->where('author', 'like', "%$query%", 'OR');
                $q->where('isbn', 'like', "%$query%", 'OR');
            })
            ->whereExists(function ($q) use ($borrowed, $member_id) {
                if ($borrowed)
                    $q->select(\DB::raw(1))
                      ->from('borrowers')
                      ->whereRaw("borrowers.book_id = books.id AND borrowers.user_id = $member_id");
            })
            ->paginate(10);

        return view('student.book.listing', [
            'books' => $books,
            'query' => $query,
            'borrowed' => (bool) $borrowed
        ]);
    }

    public function borrow($id, Request $request)
    {
        $book = BookModel::findorFail($id);

        return view('student.book.borrow', [
            'book' => $book
        ]);
    }

    public function post_borrow($id, Request $request)
    {
        $member_id = \Auth::user()->id;

        try
        {
            $book_borrow = new BorrowUsecase(
                new BorrowerModel,
                new BookModel,
                new UserModel
            );
            $book_borrow->handle($member_id, $id);

            return redirect(route('book listing'));
        }
        catch(\Exception $e)
        {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
    }

    public function surrender($id, Request $request)
    {
        $book = BookModel::findorFail($id);

        return view('student.book.surrender', [
            'book' => $book
        ]);
    }

    public function post_surrender($id, Request $request)
    {
        $member_id = \Auth::user()->id;

        try
        {
            $book_surrender = new SurrenderUsecase(
                new BorrowerModel,
                new BookModel
            );
            $book_surrender->handle($member_id, $id);

            return redirect(route('book listing'));
        }
        catch(\Exception $e)
        {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
    }
}
