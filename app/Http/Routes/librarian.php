<?php

// Members
Route::get('/admin/members', [
    'as' => 'librarian member listing',
    'uses' => 'Librarian\MemberController@listing',
    'middleware' => ['auth', 'librarian']
]);

Route::group(['prefix' => 'admin/member', 'middleware' => ['auth', 'librarian']], function() {
    Route::get('/', ['as' => 'librarian member create', 'uses' => 'Librarian\MemberController@index']);
    Route::post('/', 'Librarian\MemberController@store');

    Route::get('/{id}', ['as' => 'librarian member edit', 'uses' => 'Librarian\MemberController@edit']);
    Route::put('/{id}', ['as' => 'librarian member update', 'uses' => 'Librarian\MemberController@update']);
    Route::delete('/{id}', ['as' => 'librarian member delete', 'uses' => 'Librarian\MemberController@destroy']);
});


// Books
Route::get('/admin/books', [
    'as' => 'librarian book listing',
    'uses' => 'Librarian\BookController@listing',
    'middleware' => ['auth', 'librarian']
]);

Route::group(['prefix' => 'admin/book', 'middleware' => ['auth', 'librarian']], function() {
    Route::get('/', ['as' => 'librarian book create', 'uses' => 'Librarian\BookController@index']);
    Route::post('/', 'Librarian\BookController@store');
    Route::get('/loans', ['as' => 'librarian book loans', 'uses' => 'Librarian\BookController@loan_listing']);

    Route::get('/{id}', ['as' => 'librarian book edit', 'uses' => 'Librarian\BookController@edit']);
    Route::put('/{id}', ['as' => 'librarian book update', 'uses' => 'Librarian\BookController@update']);
    Route::get('/{id}/borrowers', ['as' => 'librarian book borrower', 'uses' => 'Librarian\BookController@borrowers']);
    Route::delete('/{id}', ['as' => 'librarian book delete', 'uses' => 'Librarian\BookController@destroy']);
});
