<?php

// Books
Route::get('/books', [
    'as' => 'book listing',
    'uses' => 'BookController@listing',
    'middleware' => ['auth', 'student']
]);

Route::group(['prefix' => 'book', 'middleware' => ['auth', 'student']], function() {
    Route::get('/borrow/{id}', ['as' => 'book borrow', 'uses' => 'BookController@borrow']);
    Route::post('/borrow/{id}', 'BookController@post_borrow');

    Route::get('/surrender/{id}', ['as' => 'book surrender', 'uses' => 'BookController@surrender']);
    Route::post('/surrender/{id}', 'BookController@post_surrender');
});