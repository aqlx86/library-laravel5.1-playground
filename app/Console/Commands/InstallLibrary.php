<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Command;

class InstallLibrary extends Command
{
    protected $signature = 'library:install';
    protected $description = 'this will run database migration and database seeder';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try
        {
            Artisan::call('migrate:install');
        }
        catch (\Illuminate\Database\QueryException $e)
        {

        }

        Artisan::call('migrate');
        Artisan::call('db:seed');

        $this->info('done!');
    }
}
