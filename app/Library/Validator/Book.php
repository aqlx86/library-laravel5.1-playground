<?php

namespace Library\Validator;

use Library\Validator;

class Book extends Validator\Validator
{
    public function create()
    {
        $this->add_rule('title', ['required']);
        $this->add_rule('author', ['required']);
        $this->add_rule('isbn', ['required', 'unique:books']);
        $this->add_rule('quantity', ['required', 'integer']);
        $this->add_rule('location', ['required']);

        return $this;
    }

    public function update()
    {
        $this->add_rule('id', ['required', 'exists:books,id']);
        $this->add_rule('title', ['required']);
        $this->add_rule('author', ['required']);
        $this->add_rule('isbn', ['required', 'unique:books,id,'.$this->get_input('id')]);
        $this->add_rule('quantity', ['required', 'integer']);
        $this->add_rule('location', ['required']);

        return $this;
    }
}