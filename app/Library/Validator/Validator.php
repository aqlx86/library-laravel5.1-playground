<?php

namespace Library\Validator;

class Validator
{
    protected $input;
    protected $rules;
    protected $errors;
    protected $validator;

    public function setup(array $input)
    {
        $this->input = $input;
        $this->rules = [];
    }

    public function add_rule($key, $rules)
    {
        if (! isset($this->rules[$key]))
            $this->rules[$key] = [];

        if (is_array($rules))
            $this->rules[$key] = array_merge($this->rules[$key], $rules);
        else
            $this->rules[$key][] = $rules;
    }

    public function is_valid()
    {
        $this->validator = \Validator::make($this->input, $this->rules, $this->messages());

        if ($this->validator->fails())
        {
            $this->errors = $this->validator->errors()->all();
            return false;
        }

        return true;
    }

    public function get_validator()
    {
        return $this->validator;
    }

    public function add_input($key, $value)
    {
        $this->input[$key] = $value;
    }

    public function get_input($key)
    {
        return isset($this->input[$key]) ? $this->input[$key] : null;
    }

    public function get_rules()
    {
        return $this->rules;
    }

    public function get_errors()
    {
        return $this->errors;
    }

    private function messages()
    {
        return [];
    }
}
