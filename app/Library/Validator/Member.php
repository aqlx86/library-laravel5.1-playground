<?php

namespace Library\Validator;

use Library\Validator;

class Member extends Validator\Validator
{
    public function create()
    {
        $this->add_rule('name', ['required']);
        $this->add_rule('age', ['required', 'integer']);
        $this->add_rule('email', ['required', 'email', 'unique:users']);
        $this->add_rule('password', ['required', 'confirmed', 'min:6']);

        return $this;
    }

    public function update()
    {
        $this->add_rule('id', ['required', 'exists:users,id']);
        $this->add_rule('name', ['required']);
        $this->add_rule('age', ['required', 'integer']);
        $this->add_rule('email', ['required', 'email', 'unique:users,id,'.$this->get_input('id')]);
        $this->add_rule('password', ['sometimes', 'confirmed', 'min:6']);

        return $this;
    }

}