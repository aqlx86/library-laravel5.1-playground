<?php

namespace Library\Helper;

use Carbon\Carbon;

class LoanHelper
{
    public static function compute_penalty($expired_at)
    {
        $now = Carbon::now();

        $expired_at = new Carbon($expired_at);

        if ($now->gt($expired_at))
        {
            $rate = 2 / 1440;

            $minutes = $now->diffInMinutes($expired_at);

            return $rate * $minutes;
        }


        return 0;
    }
}