<?php

namespace Library\Repository;

interface Book
{
    public function create_book(array $book);
    public function update_book($id, array $book);
    public function get_book($book_id);
    public function increase_quantity($book_id, $count = 1);
    public function decrease_quantity($book_id, $count = 1);
}