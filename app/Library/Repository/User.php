<?php

namespace Library\Repository;

interface User
{
    public function get_user($user_id);
    public function create_user(array $user);
    public function update_user($id, array $user);
}