<?php 

namespace Library\Repository\Book;

interface Borrow
{
    public function borrow_book($user_id, $book_id);
    public function count_book_in_hand($user_id);
}