<?php 

namespace Library\Repository\Book;

interface Surrender
{
    public function surrender_book($user_id, $book_id);
}