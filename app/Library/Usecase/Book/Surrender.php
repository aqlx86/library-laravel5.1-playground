<?php 

namespace Library\Usecase\Book;

use Library\Repository\Book as BookRepository;
use Library\Repository\Book\Surrender as BookSurrenderRepository;

class Surrender 
{
    protected $book_id;
    protected $user_id;
    protected $repository;
    protected $book_repository;

    public function __construct(BookSurrenderRepository $repository, BookRepository $book_repository)
    {
        $this->repository = $repository;
        $this->book_repository = $book_repository;
    }

    public function handle($user_id, $book_id)
    {
        $this->user_id = $user_id;
        $this->book_id = $book_id;

        $this->validate();

        $this->repository->surrender_book($user_id, $book_id);
        $this->book_repository->increase_quantity($book_id);
    }

    protected function validate()
    {
        $is_in_hand = $this->repository
            ->where('user_id', '=', $this->user_id)
            ->where('book_id', '=', $this->book_id)
            ->first();

        if (! $is_in_hand)
            throw new \Exception('You do not have this book');
    }
}