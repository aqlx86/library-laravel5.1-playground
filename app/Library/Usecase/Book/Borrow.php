<?php 

namespace Library\Usecase\Book;

use Library\Repository\Book as BookRepository;
use Library\Repository\Book\Borrow as BookBorrowRepository;
use Library\Repository\User as UserRepository;

class Borrow
{
    protected $book_id;
    protected $user_id;
    protected $repository;
    protected $book_repository;
    protected $user_repository;

    public function __construct(BookBorrowRepository $repository, BookRepository $book_repository, UserRepository $user_repository)
    {
        $this->repository = $repository;
        $this->book_repository = $book_repository;
        $this->user_repository = $user_repository;
    }

    public function handle($user_id, $book_id)
    {
        $this->user_id = $user_id;
        $this->book_id = $book_id;

        $this->validate();

        $this->repository->borrow_book($user_id, $book_id);
        $this->book_repository->decrease_quantity($book_id);
    }

    protected function validate()
    {
        $is_already_borrowed = $this->repository
            ->where('user_id', '=', $this->user_id)
            ->where('book_id', '=', $this->book_id)
            ->first();

        if ($is_already_borrowed)
            throw new \Exception('You already have this book');

        $book = $this->book_repository->get_book($this->book_id);

        if ($book['quantity'] == 0)
            throw new \Exception('Book run out of quantity');

        $number_of_book_in_hand = $this->repository
            ->count_book_in_hand($this->user_id);

        $user = $this->user_repository
            ->get_user($this->user_id);

        if ($number_of_book_in_hand >= 6 && $user['age'] > 12)
            throw new \Exception('For adult, You can only borrow 6 books max');
        
        if ($number_of_book_in_hand >= 3 && $user['age'] <= 12)
            throw new \Exception('For Junior, You can only borrow 3 books max');
    }
}