<?php

namespace Library\Usecase\Book;

use Library;
use Library\Repository\Book as BookRepository;

class Create
{
    protected $repository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(array $book)
    {
        $this->repository
            ->create_book($book);
    }
}