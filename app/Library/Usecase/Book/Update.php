<?php

namespace Library\Usecase\Book;

use Library;
use Library\Repository\Book as BookRepository;

class Update
{
    protected $repository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle($id, array $book)
    {
        $this->repository
            ->update_book($id, $book);
    }
}