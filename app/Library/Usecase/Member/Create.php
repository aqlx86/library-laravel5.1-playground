<?php

namespace Library\Usecase\Member;

use Library\Repository\User as UserRepository;

class Create
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(array $user)
    {
        $this->repository->create_user($user);
    }
}