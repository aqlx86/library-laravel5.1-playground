<?php

namespace Library\Usecase\Member;

use Library\Repository\User as UserRepository;

class Update
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle($id, array $user)
    {
        $user = array_filter($user);

        $input = [
            'name' => $user['name'],
            'age' => $user['age'],
            'email' => $user['email'],
        ];

        if (isset($user['password']))
            $input['password'] = bcrypt($user['password']);


        $this->repository->update_user($id, $input);
    }
}