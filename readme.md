## Library - Plus65

Technical Assignment from Plus65, to see it live follow this link -> [http://library-plus65.herokuapp.com/](http://library-plus65.herokuapp.com/)

### Installation
> Note: You need to create the database first before the commands below.

Run dependency tool
```
$ composer.phar install
```
Run database migration and seeder
```
$ php artisan migrate:install
$ php artisan migrate
$ php artisan db:seed

# Or this single command
$ php artisan library:install
```